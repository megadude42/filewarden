from msvcrt import getch, kbhit, putch
from os import get_terminal_size, system, getcwd, chdir, scandir, startfile, mkdir, rmdir, remove
from math import ceil, floor
from sys import exit, stdout, argv
from json import load
from codecs import escape_decode as ed
from re import finditer, compile
import signal

def jsonParse(jsonFile, jsonPath):
    data = jsonFile
    if jsonPath != "":
        for name in jsonPath.split(';'):
            if name.isdigit():
                data = data[int(name)]
            else:
                data = data[name]
    return data

class screen():
    lines = [""]
    maxWidth = lambda self : get_terminal_size()[0]
    maxHeight = lambda self : get_terminal_size()[1]
    offset = 0
    
    configFile = load(open("C:\\Program Files\\FileWarden\\FileWarden.cfg", "r"))
    configPath = ""
    
    fileTypes = jsonParse(configFile, "fileTypes").keys()
    fileHighlights = []
    
    cCheck = False

    fileName = ""
    screenMode = "File Browser"
    menuOn = False
    menuCursor = 0
    
    menuTextMode = False
    menuText = ""
    menuTextCursor = 0
    
    menuPromptMode = False
    menuPrompt = False
    menuPromptText = ""
    menuPromptCommand = ""
    
    searchText = ""
    
    fileCommands = ["Return", "Jump", "File Browse", "Save File", "Save File As", "Options", "Quit"]
    browseCommands = ["Return", "New File", "New Folder", "Options", "Quit"]
    optionCommands = ["Return", "Save Config", "File Browse", "Quit"]
    commands = {}
    
    screenTypes = {"File View": fileCommands, "File Browser": browseCommands, "Options": optionCommands}
    
    menuCommands = lambda self : self.screenTypes[self.screenMode]
    
    debugOn = True
    debugText = ""
    debugRender = lambda self : " " + self.debugText if self.debugOn else ""
    debugFile = open("logFile.txt", "a")
    
    cursorLine = 0
    cursorColumn = 0
    
    rTop = 255
    rDiff = 42
    gTop = 164
    gDiff = 140
    bTop = 38
    bDiff = 36
    
    cursorStart = "\x1b[30m\x1b[102m"
    cursorEnd =   "\x1b[92m\x1b[40m"
    
    colorCursor = lambda self, percent : "\x1b[48;2;{0};{1};{2}m\x1b[38;2;37;3;65m".format(self.rTop-round(self.rDiff*percent), self.gTop-round(self.gDiff*percent), self.bTop+round(self.bDiff*percent))
    colorBack = lambda self, percent : "\x1b[38;2;{0};{1};{2}m\x1b[49m".format(self.rTop-round(self.rDiff*percent), self.gTop-round(self.gDiff*percent), self.bTop+round(self.bDiff*percent))
    
    def __init__(self):
        system("cls")
        commandMap = {'left': self.curLeft, 'up': self.curUp, 'down': self.curDown, 'right': self.curRight, 'home': self.curHome, 'end': self.curEnd, 'back': self.backSpace, 'tab': self.tab, 'delete': self.delete, 'return': self.enter, 'undo': self.undo, 'menu': self.menu, 'exit': exit}
        keyBinds = jsonParse(self.configFile, "keyBinds")
        for key, bind in keyBinds.items():
            self.commands[ed(key.encode("utf-8"))[0]] = commandMap[bind]
    
    def ctrlC(self, x, y):
        self.cCheck = True
    
    def render(self):
        lineCount = 0
        lineNum = 0
        if self.screenMode == "File View":
            headerLine = "\033[1;1H" + self.colorBack(0) + ((self.fileName if self.fileName != "" else "Unsaved File") + self.debugRender()).ljust(self.maxWidth(), " ")
            screenText = []
            screenPreText = []
            linePre = "[*] "
            maxDepth = len(str(len(self.lines)-1))
            extendedLine = None
            extendedLen = 0
            
            for x in range(0, self.maxHeight() - 1):
                screenLine = ""
                
                if not (lineNum + self.offset in list(range(len(self.lines)))):
                    screenLine = " " * (self.maxWidth() - 6 - maxDepth)
                    screenPreText.append([-1, screenLine])
                    continue
                
                line = self.lines[lineNum + self.offset]
                indentCheck = line.replace(" ", "·").ljust(self.maxWidth() - (6 + maxDepth), " ")
                for charNum, charCheck in enumerate(indentCheck):
                    if charCheck == "·":
                        if charNum % 4 == 0 and charNum != 0:
                            indentCheck = indentCheck[:charNum] + "|" + indentCheck[charNum+1:]
                    else:
                        break
                
                if extendedLine != None:
                    lineFin = False
                    if len(indentCheck) > (self.maxWidth() - 6 - maxDepth)*(extendedLen+1):
                        screenLine += indentCheck[(self.maxWidth() - 6 - maxDepth)*extendedLen:(self.maxWidth() - 6 - maxDepth)*(extendedLen+1)]
                    else:
                        screenLine += indentCheck[(self.maxWidth() - 6 - maxDepth)*extendedLen:]
                        screenLine += "".ljust((self.maxWidth() - 6 - maxDepth) - len(screenLine), " ")
                        lineFin = True
                    
                    screenPreText.append([lineNum + self.offset, screenLine])
                    if lineFin:
                        lineNum += 1
                        extendedLine = None
                    
                    extendedLen += 1
                elif (len(indentCheck) / (self.maxWidth() - 6 - maxDepth)) > 1:
                    extendedLine = lineNum
                    screenLine += indentCheck[:self.maxWidth() - 6 - maxDepth]
                    extendedLen = 1
                    screenPreText.append([lineNum + self.offset, screenLine])
                elif (len(indentCheck) / (self.maxWidth() - 6 - maxDepth)) == 1 and not indentCheck.endswith(" "):
                    extendedLine = lineNum
                    screenLine += indentCheck[:self.maxWidth() - 6 - maxDepth]
                    extendedLen = 1
                    screenPreText.append([lineNum + self.offset, screenLine])
                else:
                    screenLine += indentCheck
                    screenPreText.append([lineNum + self.offset, screenLine])
                    lineNum += 1
            
            # for x in range(0, self.maxHeight() - 1):
                # percent = x/self.maxHeight()
                # screenLine = ""
                # if not (lineNum + self.offset in list(range(len(self.lines)))):
                    # screenLine = "[*] ".replace("*", "-"*maxDepth) + " " * (self.maxWidth() - 3 - maxDepth)
                    # colorInd = screenLine.find("]") + 1
                    # screenText.append(self.colorBack(percent) + screenLine[:colorInd] + "\x1b[97m\x1b[49m" + screenLine[colorInd:])
                    # continue
                # line = self.lines[lineNum + self.offset]
                # indentCheck = line.replace(" ", "·").ljust(self.maxWidth() - (4 + maxDepth), " ")
                # for charNum, charCheck in enumerate(indentCheck):
                    # if charCheck == "·":
                        # if charNum % 4 == 0 and charNum != 0:
                            # indentCheck = indentCheck[:charNum] + "│" + indentCheck[charNum+1:]
                    # else:
                        # break
                
                # if extendedLine != None:
                    # screenLine += linePre.replace("*", "="*maxDepth)
                    # lineFin = False
                    # if len(indentCheck) > (self.maxWidth() - 3 - maxDepth)*(extendedLen+1):
                        # screenLine += indentCheck[(self.maxWidth() - 3 - maxDepth)*extendedLen:(self.maxWidth() - 3 - maxDepth)*(extendedLen+1)]
                    # else:
                        # screenLine += indentCheck[(self.maxWidth() - 3 - maxDepth)*extendedLen:]
                        # screenLine += "".ljust(self.maxWidth() - len(screenLine), " ")
                        # lineFin = True
                    
                    # if lineNum == self.cursorLine and self.cursorColumn in list(range((self.maxWidth() - 3 - maxDepth)*extendedLen, (self.maxWidth() - 3 - maxDepth)*(extendedLen+1))):
                                               # #"\x1b[107m\x1b[30m" + \
                                               # #"\x1b[97m\x1b[49m" + \
                        # screenLine = screenLine[:self.cursorColumn + 3 + maxDepth - ((self.maxWidth() - 3 - maxDepth)*extendedLen)] + \
                                               # self.colorCursor(percent) + \
                                               # screenLine[self.cursorColumn + 3 + maxDepth - ((self.maxWidth() - 3 - maxDepth)*extendedLen)] + \
                                               # self.colorBack(percent) + \
                                               # screenLine[self.cursorColumn + 4 + maxDepth - ((self.maxWidth() - 3 - maxDepth)*extendedLen):]
                    
                    # if lineFin:
                        # lineNum += 1
                        # extendedLine = None
                    
                    # extendedLen += 1
                
                # elif (len(indentCheck) / (self.maxWidth() - 3 - maxDepth)) >= 1:
                    # extendedLine = lineNum
                    # screenLine += linePre.replace("*", str(lineNum + self.offset).rjust(maxDepth))
                    # screenLine += indentCheck[:self.maxWidth() - 3 - maxDepth]
                    # if lineNum == self.cursorLine and self.cursorColumn in list(range(0, self.maxWidth() - 3 - maxDepth)):
                        # if len(screenLine) == self.cursorColumn + 3 + maxDepth:
                            # #screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + "\x1b[107m\x1b[30m" + " " + "\x1b[97m\x1b[49m" + screenLine[self.cursorColumn + 4 + maxDepth:]
                            # screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + self.colorCursor(percent) + " " + self.colorBack(percent) + screenLine[self.cursorColumn + 4 + maxDepth:]
                        # else:
                            # #screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + "\x1b[107m\x1b[30m" + screenLine[self.cursorColumn + 3 + maxDepth] + "\x1b[97m\x1b[49m" + screenLine[self.cursorColumn + 4 + maxDepth:]
                            # screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + self.colorCursor(percent) + screenLine[self.cursorColumn + 3 + maxDepth] + self.colorBack(percent) + screenLine[self.cursorColumn + 4 + maxDepth:]
                    # extendedLen = 1
                
                # else:
                    # screenLine += linePre.replace("*", str(lineNum + self.offset).rjust(maxDepth))
                    # screenLine += indentCheck
                    # if lineNum == self.cursorLine:
                        # if len(screenLine) == self.cursorColumn + 3 + maxDepth:
                            # #screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + "\x1b[107m\x1b[30m" + " " + "\x1b[97m\x1b[49m" + screenLine[self.cursorColumn + 4 + maxDepth:]
                            # screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + self.colorCursor(percent) + " " + self.colorBack(percent) + screenLine[self.cursorColumn + 4 + maxDepth:]
                        # else:
                            # #screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + "\x1b[107m\x1b[30m" + screenLine[self.cursorColumn + 3 + maxDepth] + "\x1b[97m\x1b[49m" + screenLine[self.cursorColumn + 4 + maxDepth:]
                            # screenLine = screenLine[:self.cursorColumn + 3 + maxDepth] + self.colorCursor(percent) + screenLine[self.cursorColumn + 3 + maxDepth] + self.colorBack(percent) + screenLine[self.cursorColumn + 4 + maxDepth:]
                    # screenLine += "".ljust(self.maxWidth() - len(screenLine), " ")
                    # lineNum += 1
                # colorInd = screenLine.find("]") + 1
                # screenText.append(self.colorBack(percent) + screenLine[:colorInd] + "\x1b[97m\x1b[49m" + screenLine[colorInd:])
                
                
                
            
            logicalCurColumn = self.cursorColumn
            cursorPlaced = False
            lastLine = None
            
            for ind, line in enumerate(screenPreText):
                tokens = []
                colorList = {}
                cursorList = {}
                for pattern, colorCode in self.fileHighlights:
                    for match in finditer(pattern, line[1]):
                        start, end = match.start(1), match.end(1)
                        overlap = False

                        for token in tokens:
                            if start in list(range(token[0], token[1]+1)):
                                overlap = True
                                break
                        
                        if not overlap:
                            colorList[start] = colorCode
                            colorList[end] = "\x1b[97m\x1b[49m"
                            tokens.append((start, end, colorCode))
                        
                
                if line[0] - self.offset == self.cursorLine:
                    if logicalCurColumn >= len(line[1]):
                        logicalCurColumn -= len(line[1])
                    else:
                        if not cursorPlaced:
                            cursorPos = (logicalCurColumn, logicalCurColumn+1, self.colorCursor(ind/self.maxHeight()))
                            cursorList[logicalCurColumn] = "cursorStart"
                            cursorList[logicalCurColumn+1] = "cursorEnd"
                            cursorPlaced = True
                else:
                    cursorPlaced = False
                
                lineReplacement = ""
                lastColor = "\x1b[97m\x1b[49m"
                for charInd, charSelect in enumerate(line[1]):
                    if colorList.get(charInd) != None:
                        lastColor = colorList.get(charInd)
                        lineReplacement += lastColor.encode().decode("unicode_escape")
                    
                    if cursorList.get(charInd) != None:
                        if cursorList.get(charInd) == "cursorStart":
                            lineReplacement += self.colorCursor(ind/self.maxHeight())
                        elif cursorList.get(charInd) == "cursorEnd":
                            lineReplacement += lastColor.encode().decode("unicode_escape")
                    
                    lineReplacement += charSelect
                
                if lastLine == line[0] and line[0] != -1:
                    prefix = linePre.replace("*", "="*maxDepth)
                elif line[0] == -1:
                    prefix = linePre.replace("*", "-"*maxDepth)
                else:
                    prefix = linePre.replace("*", str(line[0]).rjust(maxDepth))
                
                lineReplacement = self.colorBack(ind/self.maxHeight()) + prefix + "\x1b[97m\x1b[49m" + lineReplacement
                screenText.append(lineReplacement)
                lastLine = line[0]
            
            
            screenText = [headerLine] + screenText[:]
            
        elif self.screenMode == "File Browser":
            prefixes = ["[{}] ", "[==] ", "[##] "]
            dirList = [[0, ".."]]
            fileList = []
            
            permissionCheck = True
            while True:
                try:
                    for fileObj in scandir():
                        if fileObj.is_dir():
                            dirList.append([0, fileObj.name])
                        if fileObj.is_file():
                            fileList.append([1, fileObj.name])
                        if fileObj.is_symlink():
                            fileList.append([2, fileObj.name])
                    break
                except PermissionError:
                    chdir("..")
                    permissionCheck = False
                
            screenText = [self.colorBack(0) + "\033[1;1H" + (getcwd() + " " + self.searchText + self.debugRender()).ljust(self.maxWidth(), " ")]
            self.lines = dirList + fileList
            
            if not permissionCheck:
                self.lines[0][1] += " [NO PERMISSIONS]"
            
            for x in range(0, self.maxHeight() - 1):
                percent = x/self.maxHeight()
                screenLine = self.colorBack(percent)
                if not (lineNum + self.offset in list(range(len(self.lines)))):
                    screenText.append(self.colorBack(percent) + "[--] " + " " * (self.maxWidth() - 5))
                    continue
                else:
                    screenLine += prefixes[self.lines[x + self.offset][0]]
                    if lineNum == self.cursorLine:
                        screenLine += self.colorCursor(percent) + self.lines[x + self.offset][1] + self.colorBack(percent) + "".ljust(self.maxWidth() - 5 - len(self.lines[x + self.offset][1]))
                    else:
                        screenLine += self.lines[x + self.offset][1].ljust(self.maxWidth() - 5)
                lineNum += 1
                screenText.append(screenLine)
        elif self.screenMode == "Options": 
            screenText = [self.colorBack(0) + "\033[1;1H" + ("Options:" + self.debugRender()).ljust(self.maxWidth(), " ")]
            
            prefixes = ["[{}] ", "[##] ", "[==] ", "[..] "]
            configList = [[3, ".."]]
            
            for key, option in jsonParse(self.configFile, self.configPath).items():
                if type(option) == dict:
                    configList.append([0, key])
                if type(option) == list:
                    configList.append([1, key])
                if type(option) == str:
                    configList.append([2, key])
            
            self.lines = configList[:]
            
            for x in range(0, self.maxHeight() - 1):
                percent = x/self.maxHeight()
                screenLine = self.colorBack(percent)
                if not (lineNum + self.offset in list(range(len(self.lines)))):
                    screenText.append(self.colorBack(percent) + "[--] " + " " * (self.maxWidth() - 5))
                    continue
                else:
                    screenLine += prefixes[self.lines[x + self.offset][0]]
                    if lineNum == self.cursorLine:
                        screenLine += self.colorCursor(percent) + str(self.lines[x + self.offset][1]) + self.colorBack(percent) + "".ljust(self.maxWidth() - 5 - len(str(self.lines[x + self.offset][1])))
                    else:
                        screenLine += str(self.lines[x + self.offset][1]).ljust(self.maxWidth() - 5)
                lineNum += 1
                screenText.append(screenLine)
        
        if self.menuOn:
            screenText[-2] = self.colorBack(1) + ("=" * self.maxWidth())
            menuLine = ""
            if self.menuPromptMode:
                menuLine += self.menuPromptText
                menuEnd = ": Yes | No"
                menuSpacer = "".ljust(self.maxWidth() - len(menuLine + menuEnd))
                if self.menuPrompt:
                    menuEnd = menuEnd[:-8] + self.colorCursor(1) + menuEnd[-8:-5] + self.colorBack(1) + menuEnd[-5:]
                else:
                    menuEnd = menuEnd[:-2] + self.colorCursor(1) + menuEnd[-2:] + self.colorBack(1)
                menuLine += menuEnd + menuSpacer
            elif self.menuTextMode:
                menuLine += self.menuCommands()[self.menuCursor] + ": " + self.menuText + " "
                menuLine = menuLine.ljust(self.maxWidth())
                cursorLoc = self.menuTextCursor + len(self.menuCommands()[self.menuCursor] + ": ")
                menuLine = menuLine[:cursorLoc] + self.colorCursor(1) + menuLine[cursorLoc] + self.colorBack(1) + menuLine[cursorLoc+1:]
            else:
                for option in self.menuCommands()[:self.menuCursor]:
                    menuLine += "\b| {} |".replace("{}", option)
                menuLine += "\b|%{}%|".replace("{}", self.menuCommands()[self.menuCursor])
                for option in self.menuCommands()[self.menuCursor+1:]:
                    menuLine += "\b| {} |".replace("{}", option)
                menuLine = menuLine.ljust(self.maxWidth() + (menuLine.count("\b")), " ")
                menuLine = menuLine.replace("|%", "| " + self.colorCursor(1)).replace("%|", self.colorBack(1) + " |")
            screenText[-1] = menuLine
        
        if self.debugOn:
            self.debugFile.write(self.debugText + "\n")
        
        return "\n".join(screenText)
    
    def menu(self):
        self.menuOn = not self.menuOn
        self.menuCursor = 0
    def menuJump(self):
        if self.menuTextMode:
            try:
                lineNum = int(self.menuText)
                if lineNum >= 0 and lineNum < len(self.lines):
                    self.offset = lineNum
                    self.cursorLine = 0
                    self.cursorColumn = 0
                self.menuTextMode = False
                self.menuOn = False
            except:
                self.menuTextMode = False
                self.menuOn = False
        else:
            self.menuTextMode = True
            self.menuTextCursor = 0
            self.menuText = ""
    def menuBrowse(self):
        self.screenMode = "File Browser"
        self.offset = 0
        self.cursorColumn = 0
        self.cursorLine = 0
        self.menuOn = False
    def menuSave(self):
        if self.fileName == "" and not self.menuTextMode:
            self.menuTextMode = True
            self.menuText = ""
            self.menuTextCursor = 0
        elif self.fileName == "":
            file = open(self.menuText, "w")
            file.write("\n".join(self.lines))
            file.close()
            self.menuTextMode = False
            self.menuOn = False
            self.screenMode = "File Browser"
            self.cursorLine = 0
            self.cursorColumn = 0
            self.offset = 0
        else:
            file = open(self.fileName, "w")
            file.write("\n".join(self.lines))
            file.close()
            self.menuTextMode = False
            self.menuOn = False
            self.screenMode = "File Browser"
            self.cursorLine = 0
            self.cursorColumn = 0
            self.offset = 0
    def menuSaveAs(self):
        if self.menuTextMode:
            file = open(self.menuText, "w")
            file.write("\n".join(self.lines))
            file.close()
            self.menuTextMode = False
            self.menuOn = False
            self.screenMode = "File Browser"
            self.cursorLine = 0
            self.cursorColumn = 0
            self.offset = 0
        else:
            self.menuTextMode = True
            self.menuText = ""
            self.menuTextCursor = 0
    def menuNewFile(self):
        self.fileName = ""
        self.lines = [""]
        self.actionStack = []
        self.cursorLine = 0
        self.cursorColumn = 0
        self.screenMode = "File View"
        self.menuTextMode = False
        self.menuOn = False
        self.offset = 0
    def menuNewFolder(self):
        if self.menuTextMode:
            mkdir(self.menuText)
            self.menuTextMode = False
            self.menuOn = False
            chdir(self.menuText)
            self.offset = 0
            self.cursorLine = 0
        else:
            self.menuTextMode = True
            self.menuTextCursor = 0
            self.menuText = ""
    def menuDelItem(self):
        if self.menuPromptMode:
            self.menuPromptMode = False
            self.menuOn = False
            if self.menuPrompt:
                if self.lines[self.cursorLine + self.offset][0] == 0 and self.lines[self.cursorLine + self.offset][1] != "..":
                    rmdir(self.lines[self.cursorLine + self.offset][1])
                elif self.lines[self.cursorLine + self.offset][0] == 1:
                    remove(self.lines[self.cursorLine + self.offset][1])
            self.offset = 0
            self.menuPromptCommand = ""
            self.menuPromptText = ""
            self.cursorLine = 0
        else:
            self.menuPromptMode = True
            self.menuOn = True
            self.menuPrompt = False
            self.menuPromptCommand = "Delete"
            self.menuPromptText = "Are you sure you want to delete (*)".replace("*", self.lines[self.cursorLine + self.offset][1])
    def menuOptions(self):
        self.screenMode = "Options"
        self.configPath = ""
        self.offset = 0
        self.cursorColumn = 0
        self.cursorLine = 0
        self.menuOn = False
    
    def text(self, charInsert):
        if self.menuTextMode:
            line = self.menuText
            cursor = self.menuTextCursor
        elif self.menuOn:
            pass
        elif self.screenMode == "File View":
            line = self.lines[self.cursorLine + self.offset]
            cursor = self.cursorColumn
        elif self.screenMode == "File Browser":
            self.searchText += charInsert
            for ind, line in enumerate(self.lines):
                if line[1].lower().startswith(self.searchText):
                    self.offset = ind
                    self.cursorLine = 0
                    return
        
        if self.screenMode != "File Browser" or self.menuTextMode:
            if line == "":
                lineText = charInsert
            elif cursor == len(line):
                lineText = line + charInsert
            else:
                lineText = line[:cursor] + charInsert + line[cursor:]
        
        if self.menuTextMode:
            self.menuText = lineText
            self.menuTextCursor += 1
        elif self.screenMode == "File View":
            self.lines[self.cursorLine + self.offset] = lineText
            self.actionStack.append(("text", self.cursorLine + self.offset, self.cursorColumn, charInsert))
            self.cursorColumn += 1
    
    def backSpace(self):
        if self.menuOn:
            if self.menuTextMode and self.menuTextCursor != 0:
                self.menuText = self.menuText[:self.menuTextCursor - 1] + self.menuText[self.menuTextCursor:]
                self.menuTextCursor -= 1
        else:
            if self.screenMode == "File Browser":
                self.searchText = ""
                return
            if self.cursorColumn == 0:
                if self.cursorLine != 0:
                    self.actionStack.append(("removeLine", self.cursorLine + self.offset - 1, len(self.lines[self.cursorLine + self.offset - 1]), ""))
                    self.cursorColumn = len(self.lines[self.cursorLine + self.offset - 1])
                    self.lines[self.cursorLine + self.offset - 1] = str(self.lines[self.cursorLine + self.offset - 1]) + str(self.lines.pop(self.cursorLine + self.offset))
                    self.cursorLine -= 1
                elif self.offset != 0:
                    self.actionStack.append(("removeLine", self.cursorLine + self.offset - 1, len(self.lines[self.cursorLine + self.offset - 1]), ""))
                    self.cursorColumn = len(self.lines[self.cursorLine + self.offset - 1])
                    self.lines[self.cursorLine + self.offset - 1] = str(self.lines[self.cursorLine + self.offset - 1]) + str(self.lines.pop(self.cursorLine + self.offset))
                    self.offset -= 1
            else:
                self.actionStack.append(("backSpace", self.cursorLine + self.offset, self.cursorColumn, self.lines[self.cursorLine + self.offset][self.cursorColumn - 1]))
                self.lines[self.cursorLine + self.offset] = str(self.lines[self.cursorLine + self.offset][:self.cursorColumn - 1]) + str(self.lines[self.cursorLine + self.offset][self.cursorColumn:])
                self.cursorColumn -= 1
    def delete(self):
        self.searchText = ""
        if self.screenMode == "File Browser":
            self.menuDelItem()
        elif self.menuOn:
            if self.menuTextMode and self.menuTextCursor != len(self.menuText):
                self.menuText = self.menuText[:self.menuTextCursor] + self.menuText[self.menuTextCursor + 1:]
        else:
            if self.cursorColumn == len(self.lines[self.cursorLine + self.offset]) and len(self.lines)-1 > self.cursorLine + self.offset:
                self.actionStack.append(("removeLine", self.cursorLine + self.offset, len(self.lines[self.cursorLine + self.offset]), ""))
                self.lines[self.cursorLine + self.offset] += self.lines.pop(self.cursorLine + self.offset + 1)
            else:
                self.actionStack.append(("delete", self.cursorLine + self.offset, self.cursorColumn, self.lines[self.cursorLine + self.offset][self.cursorColumn]))
                self.lines[self.cursorLine + self.offset] = self.lines[self.cursorLine + self.offset][:self.cursorColumn] + self.lines[self.cursorLine + self.offset][self.cursorColumn + 1:]
    def enter(self):
        self.searchText = ""
        if self.menuOn:
            if self.menuPromptMode:
                promptCommandDict = {"Delete": self.menuDelItem}
                promptCommandDict[self.menuPromptCommand]()
            else:
                commandDict = {"Return": self.menu, "Jump": self.menuJump, "File Browse": self.menuBrowse, "Save File": self.menuSave, "Save File As": self.menuSaveAs, "New File": self.menuNewFile, "New Folder": self.menuNewFolder, "Options": self.menuOptions, "Quit": exit}
                commandDict[self.menuCommands()[self.menuCursor]]()
        elif self.screenMode == "File View":
            indent = ""
            if self.lines[self.cursorLine + self.offset].startswith(" "):
                spaceCount = 0
                for spaceCheck in self.lines[self.cursorLine + self.offset]:
                    if spaceCheck == " ":
                        spaceCount += 1
                    else:
                        break
                indent += " " * spaceCount
            if self.cursorColumn == len(self.lines[self.cursorLine + self.offset]):
                self.lines.insert(self.cursorLine + self.offset + 1, indent)
            else:
                self.lines.insert(self.cursorLine + self.offset + 1, indent + self.lines[self.cursorLine + self.offset][self.cursorColumn:])
                self.lines[self.cursorLine + self.offset] = self.lines[self.cursorLine + self.offset][:self.cursorColumn]
            self.actionStack.append(("newLine", self.cursorLine + self.offset, self.cursorColumn, indent))
            if self.cursorLine + 1 == self.maxHeight() - 15:
                self.offset += 1
                self.cursorLine -= 1
            self.cursorLine += 1
            self.cursorColumn = len(indent)
        elif self.screenMode == "Options":
            if str(self.lines[self.cursorLine + self.offset][1]) == "..":
                pass
            elif self.configPath == "":
                self.configPath += str(self.lines[self.cursorLine + self.offset][1])
            else:
                self.configPath += ";" + str(self.lines[self.cursorLine + self.offset][1])
        else:
            if self.lines[self.cursorLine + self.offset][0] == 0:
                if not self.lines[self.cursorLine + self.offset][1].endswith(" [NO PERMISSIONS]"):
                    chdir(self.lines[self.cursorLine + self.offset][1])
                self.cursorLine = 0
                self.offset = 0
            elif self.lines[self.cursorLine + self.offset][0] == 1:
                extension = self.lines[self.cursorLine + self.offset][1].split(".")[-1]
                if extension in self.fileTypes:
                    file = open(self.lines[self.cursorLine + self.offset][1], "r")
                    self.fileName = self.lines[self.cursorLine + self.offset][1]
                    self.lines = file.read().split("\n")
                    file.close()
                    self.actionStack = []
                    self.screenMode = "File View"
                    self.cursorLine = 0
                    self.cursorColumn = 0
                    self.offset = 0
                    self.fileHighlights = jsonParse(self.configFile, f"fileTypes;{extension};highlights")
                else:
                    startfile(self.lines[self.cursorLine + self.offset][1])
                    self.cursorLine = 0
    def tab(self):
        if self.screenMode == "File View":
            line = self.lines[self.cursorLine + self.offset]
            if line == "":
                lineText = "    "
            elif self.cursorColumn == len(line):
                lineText = line + "    "
            else:
                lineText = line[:self.cursorColumn] + "    " + line[self.cursorColumn:]
            self.lines[self.cursorLine + self.offset] = lineText
            self.actionStack.append(("tab", self.cursorLine + self.offset, self.cursorColumn, ""))
            self.cursorColumn += 4
    def undo(self):
        if len(self.actionStack) == 0:
            return
        commandType, lineNum, column, data = self.actionStack.pop()
        if commandType == "text":
            self.lines[lineNum] = self.lines[lineNum][:column] + self.lines[lineNum][column + 1:]
            self.cursorColumn = column
        elif commandType == "backSpace":
            self.lines[lineNum] = self.lines[lineNum][:column-1] + data + self.lines[lineNum][column-1:]
            self.cursorColumn = column
        elif commandType == "delete":
            self.lines[lineNum] = self.lines[lineNum][:column] + data + self.lines[lineNum][column:]
            self.cursorColumn = column
        elif commandType == "newLine":
            self.cursorColumn = len(self.lines[lineNum])
            self.lines[lineNum] = self.lines[lineNum] + self.lines.pop(lineNum+1)[len(data):]
        elif commandType == "tab":
            self.lines[lineNum] = self.lines[lineNum][:column] + self.lines[lineNum][column + 4:]
            self.cursorColumn = column
        if lineNum > 20:
            self.offset = lineNum - 20
            self.cursorLine = 20
        else:
            self.offset = 0
            self.cursorLine = lineNum
    def curLeft(self):
        self.searchText = ""
        if self.menuOn:
            if self.menuPromptMode:
                self.menuPrompt = not self.menuPrompt
            elif self.menuTextMode:
                if self.menuTextCursor != 0:
                    self.menuTextCursor -= 1
            else:
                if self.menuCursor != 0:
                    self.menuCursor -= 1
        elif self.screenMode == "File View":
            if self.cursorColumn != 0:
                self.cursorColumn -= 1
            elif self.cursorLine != 0:
                self.cursorLine -= 1
                self.cursorColumn = len(self.lines[self.cursorLine + self.offset])
            elif self.offset != 0:
                self.offset -= 1
                self.cursorColumn = len(self.lines[self.cursorLine + self.offset])
    def curUp(self):
        self.searchText = ""
        if not self.menuOn:
            if self.cursorLine != 0:
                self.cursorLine -= 1
            elif self.offset != 0:
                self.offset -= 1
            else:
                self.cursorColumn = 0
            if len(self.lines[self.cursorLine + self.offset]) < self.cursorColumn:
                self.cursorColumn = len(self.lines[self.cursorLine + self.offset])
    def curDown(self):
        self.searchText = ""
        if not self.menuOn:
            if self.cursorLine + self.offset + 1 == len(self.lines):
                self.cursorColumn = len(self.lines[self.cursorLine])
            elif self.cursorLine + 1 == self.maxHeight() - 15:
                self.offset += 1
            else:
                self.cursorLine += 1
            if len(self.lines[self.cursorLine + self.offset]) < self.cursorColumn:
                self.cursorColumn = len(self.lines[self.cursorLine + self.offset])
    def curRight(self):
        self.searchText = ""
        if self.menuOn:
            if self.menuPromptMode:
                self.menuPrompt = not self.menuPrompt
            elif self.menuTextMode:
                if self.menuTextCursor != len(self.menuText):
                    self.menuTextCursor += 1
            else:
                if self.menuCursor != len(self.menuCommands()) - 1:
                    self.menuCursor += 1
        elif self.screenMode == "File View":
            if len(self.lines[self.cursorLine + self.offset]) != self.cursorColumn:
                self.cursorColumn += 1
            elif self.cursorLine + self.offset + 1 != len(self.lines) and self.cursorLine + 1 != self.maxHeight() - 15:
                self.cursorLine += 1
                self.cursorColumn = 0
            elif self.cursorLine + 1 == self.maxHeight() - 1:
                self.cursorColumn = 0
                self.offset += 1
    def curEnd(self):
        if self.screenMode == "File View":
            self.cursorColumn = len(self.lines[self.cursorLine + self.offset])
        elif self.screenMode == "File Browser":
            self.cursorLine = len(self.lines) - 1
    def curHome(self):
        if self.screenMode == "File View":
            if self.lines[self.cursorLine + self.offset].startswith(" "):
                start = 0
                while self.lines[self.cursorLine + self.offset][start] == " ":
                    start += 1
                if self.cursorColumn > start:
                    self.cursorColumn = start
                else:
                    self.cursorColumn = 0
            else:
                self.cursorColumn = 0
        elif self.screenMode == "File Browser":
            self.cursorLine = 0


userScreen = screen()

signal.signal(signal.SIGINT, userScreen.ctrlC)




letters = list(" zxcvbkm,./ZXCVBKM<>?arstdhneio'ARSTDHNEIO\"qwfpgjluy;[]QWFPGJLUY:{}|`1234567890-=~!@#$%^&*()_+") + ["\\\\"]

system("cls")
stdout.write(userScreen.render())

while True:
    fullChar = b""
    while not kbhit():
        if userScreen.cCheck:
            renderText = userScreen.render()
            stdout.write(renderText)
            userScreen.cCheck = False
    
    while kbhit():
        fullChar += getch()
    
    if fullChar != b"":
        if fullChar in userScreen.commands.keys():
            userScreen.commands[fullChar]()
        if str(fullChar)[2:-1] in letters:
            userScreen.text(str(fullChar)[2:-1][0])
        renderText = userScreen.render()
        stdout.write(renderText)
    
